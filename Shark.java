public class Shark{
	//Setting all fields to private
	private String food;
	private String name;
	private int biteForce;
	
	public void goEat(){
		if(this.biteForce > 500){
			System.out.println(name + " ate the " + food + " hes not hungry no more");
		}
		else{
			System.out.println(name + " didnt eat he was to weak he's still hungry");
		}
	}
	public void bite(){
		if(this.biteForce > 5000){
			System.out.print(name.toUpperCase() + " BIT YOU RUN!!! HE'S TO STRONG FOR U");
		}else{
			System.out.println(name + " tried biting you because he was hungry. It didnt hurt but hes dangerous kill him. It must be done.");
		}
	}
	/* Erased set method
	
	//Set method for food
	public void setFood(String food){
		this.food = food;
	}
	//Set method for Name
	public void setName(String name){
		this.name = name;
	}
	*/
	
	
	//Get method for food
	public String getFood(){
		return this.food;
	}
	//Get method for Name
	public String getName(){
		return this.name;
	}
	//Set method for BiteForce
	public void setBiteForce(int biteForce){
		this.biteForce = biteForce;
	}
	//Get method for Biteforce
	public int getBiteForce(){
		return this.biteForce;
	}
	public Shark (String name, String food, int biteForce) {
		this.name = name;
		this.food = food;
		this.biteForce = biteForce;
	}	
}