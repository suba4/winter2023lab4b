import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Shark[] shiver = new Shark[1];
		for(int i = 0; i < shiver.length; i++){
				
			System.out.println("Name your Shark");
			//Calling setName method to hold the name of the shark
			String name = reader.nextLine();
			
			//Change the shiver[i].setName so that it calls the method to get the name instead of calling a private field
			System.out.println(name + " is hungry what should he eat?");
			
			//Calling setFood method to hold the name of the food
			String food = reader.nextLine();
			System.out.println("chose your sharks bite force");
			
			//Calling setBiteForce method to hold the int of it's bite force
			int biteForce = Integer.parseInt(reader.nextLine());
			
			shiver[i] = new Shark(name, food, biteForce);
		}
		//Print value of the last shark
		System.out.println("Your last sharks name is " + shiver[shiver.length-1].getName());
		System.out.println("Your last sharks food is " +shiver[shiver.length-1].getFood());
		System.out.println("Your last sharks biteforce is " + shiver[shiver.length-1].getBiteForce());
		
		
		//Ask new biteforce value
		System.out.println("chose your last sharks bite force");
		shiver[shiver.length-1].setBiteForce(Integer.parseInt(reader.nextLine()));
		//Print all old info and new biteforce
		System.out.println("Your last sharks name is " +shiver[shiver.length-1].getName());
		System.out.println("Your last sharks food is " +shiver[shiver.length-1].getFood());
		System.out.println("Your last sharks biteforce is " + shiver[shiver.length-1].getBiteForce());
		
	}
}